package main

import (
	"fmt"
	"sync"
)

func main() {
	cancelCh := make(chan bool)
	dataCh := make(chan int)
	wg := &sync.WaitGroup{}

	wg.Add(2)

	go func(cancelCh chan bool, dataCh chan int) {
		defer wg.Done()
		val := 0
		for {
			select {
			case <-cancelCh:
				return
			case dataCh <- val:
				val++
			}
		}
	}(cancelCh, dataCh)

	go func(cancelCh chan bool, dataCh chan int) {
		defer wg.Done()
		val := 0
		for {
			select {
			case <-cancelCh:
				return
			case dataCh <- val:
				val++
			}
		}
	}(cancelCh, dataCh)

	go func() {
		fmt.Println("before Wait")
		wg.Wait()
		fmt.Println("close dataCh")
		close(dataCh)
	}()

	// поколдовать с завершением 2-х горутин и закрытием канала dataCh

	for curVal := range dataCh {
		fmt.Println("read", curVal)
		if curVal > 3 {
			fmt.Println("send cancel")
			cancelCh <- true
			cancelCh <- true
			// break
		}
	}

}
