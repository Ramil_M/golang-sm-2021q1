package main

import (
	"fmt"
)

var idx = 1

func abc(i int) {
	fmt.Println(idx, i)
}

func main() {
	for i := 0; i < 5; i++ {
		go abc(i)
	}
	fmt.Scanln()
}
