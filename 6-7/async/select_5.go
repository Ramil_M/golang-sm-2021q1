package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

type Canceler struct {
	Val uint32
	Ch  chan struct{}
}

func (c *Canceler) Cancel() {
	for i := uint32(0); i < c.Val; i++ {
		c.Ch <- struct{}{}
	}
}

func (c *Canceler) Done() chan struct{} {
	atomic.AddUint32(&c.Val, 1)
	return c.Ch
}

func main() {
	cancelCh := make(chan bool)
	dataCh := make(chan int)
	wg := &sync.WaitGroup{}

	wg.Add(2)

	cancel := &Canceler{Ch: make(chan struct{})}

	go func(cancelCh chan bool, dataCh chan int) {
		defer wg.Done()
		val := 0
		done := cancel.Done()
		for {
			select {
			case <-done:
				fmt.Println("get cancel")
				return
			case dataCh <- val:
				val++
			}
		}
	}(cancelCh, dataCh)

	go func(cancelCh chan bool, dataCh chan int) {
		defer wg.Done()
		val := 0
		done := cancel.Done()
		for {
			select {
			case <-done:
				fmt.Println("get cancel")
				return
			case dataCh <- val:
				val++
			}
		}
	}(cancelCh, dataCh)

	go func() {
		fmt.Println("before Wait")
		wg.Wait()
		fmt.Println("close dataCh")
		close(dataCh)
	}()

	// поколдовать с завершением 2-х горутин и закрытием канала dataCh

	for curVal := range dataCh {
		fmt.Println("read", curVal)
		if curVal > 3 {
			fmt.Println("send cancel")
			cancel.Cancel()
			// break
		}
	}

}
