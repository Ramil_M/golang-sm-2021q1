package main

import (
	"log"
	"time"
)

func main() {
	result := make(chan string)
	result2 := make(chan string)

	go func(out chan<- string) {
		time.Sleep(1 * time.Second)
		log.Println("async operation ready, return result")
		out <- "success"
	}(result)

	go func(out chan<- string) {
		time.Sleep(1 * time.Second)
		log.Println("async operation ready, return result")
		out <- "success2"
	}(result2)

	time.Sleep(2 * time.Second)
	log.Println("some userful work")

	done := make(chan struct{}, 1)
	idx := 0
LOOP:
	for {
		if idx >= 2 {
			done <- struct{}{}
		}
		select {
		case <-result:
			idx++
		case <-result2:
			idx++
		case <-done:
			break LOOP
		}
	}
	// opStatus := <-result
	// log.Println("main goroutine:", opStatus)
}
