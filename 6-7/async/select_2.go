package main

import (
	"fmt"
	"time"
)

func main() {
	ch1 := make(chan int, 2)
	ch2 := make(chan int, 2)

	go func() {
		ch1 <- 1
		time.Sleep(2 * time.Millisecond)
		ch1 <- 2
		ch2 <- 3
	}()
	time.Sleep(1 * time.Millisecond)
LOOP:
	for {
		select {
		case v1 := <-ch1:
			fmt.Println("chan1 val", v1)
		case v2 := <-ch2:
			fmt.Println("chan2 val", v2)
		default:
			fmt.Println("default")
			break LOOP
		}
	}
}
