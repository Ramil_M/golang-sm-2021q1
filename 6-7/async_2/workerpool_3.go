package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func worker(ctx context.Context, idx int, jobCh chan func(), cancelCh chan struct{}, wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Println(idx, "worker start")
	for {
		select {
		case <-ctx.Done():
			fmt.Println(idx, "worker stopped by context")
			return
		case <-cancelCh:
			fmt.Println(idx, "worker stopped")
			return
		case jobToDo := <-jobCh:
			fmt.Println(idx, "worker start job")
			jobToDo()
			// dont do this!!!!!
		}
	}
	fmt.Println("end")
}

func main() {

	ctx, finish := context.WithCancel(context.Background())

	job := func() {
		waitTime := time.Duration(rand.Intn(10)+10) * time.Millisecond
		time.Sleep(waitTime)
		fmt.Println("was sleeping", waitTime)
	}

	var (
		jobCh    = make(chan func(), 1000)
		cancelCh = make(chan struct{}, 1000)
		wg       = &sync.WaitGroup{}
	)

	var idx = 0
	for ; idx < 10; idx++ {
		wg.Add(1)
		go worker(ctx, idx, jobCh, cancelCh, wg)
	}

	go func() {
		for {
			time.Sleep(time.Millisecond)
			jobCh <- job
		}
	}()

	go func() {
		result := make(chan int, 1)
		waitTime := time.Duration(rand.Intn(20)+10) * time.Millisecond
		time.Sleep(waitTime)

		jobCh <- func() {
			result <- 100000
		}
		fmt.Println("result", <-result)
		// finish()
	}()

	go func() {
		waitTime := time.Duration(rand.Intn(20)+10) * time.Millisecond
		time.Sleep(waitTime)

		posts := 0
		comments := 0

		wg := &sync.WaitGroup{}
		wg.Add(1)
		jobCh <- func() {
			defer wg.Done()
			posts = 23
		}

		wg.Add(1)
		jobCh <- func() {
			defer wg.Done()
			comments = 14
		}
		wg.Wait()

		finish()
	}()

	wg.Wait()

}
