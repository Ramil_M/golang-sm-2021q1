package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func worker(idx int, jobCh chan func(), cancelCh chan struct{}, wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Println(idx, "worker start")
	for {
		select {
		case <-cancelCh:
			fmt.Println(idx, "worker stopped")
			return
		case jobToDo := <-jobCh:
			fmt.Println(idx, "worker start job")
			jobToDo()
			// dont do this!!!!!
			// default:
			// 	// nothing
		}
	}
	fmt.Println("end")
}

func main() {

	job := func() {
		waitTime := time.Duration(rand.Intn(1000)+10) * time.Millisecond
		time.Sleep(waitTime)
		fmt.Println("was sleeping", waitTime)
	}

	var (
		jobCh    = make(chan func(), 1000)
		cancelCh = make(chan struct{}, 1000)
		wg       = &sync.WaitGroup{}
	)

	for i := 0; i < 900; i++ {
		jobCh <- job
	}
	fmt.Println(len(jobCh))

	var idx = 0
	for ; idx < 10; idx++ {
		wg.Add(1)
		go worker(idx, jobCh, cancelCh, wg)
	}

	go func() {
		for range time.Tick(time.Second) {
			if getCPU < 80 {
				wg.Add(1)
				go worker(idx, jobCh, cancelCh, wg)
			}
		}
	}()

	fmt.Println(len(jobCh))

	time.Sleep(20 * time.Second)
	for i := 0; i < idx; i++ {
		cancelCh <- struct{}{}
	}

	wg.Wait()

}
