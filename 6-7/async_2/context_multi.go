package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	workTime := 1000 * time.Millisecond
	ctx0, finish := context.WithTimeout(context.Background(), workTime)

	ctx1, _ := context.WithTimeout(ctx0, 5000*time.Millisecond)

	go func() {
		select {
		case <-time.After(500 * time.Millisecond):
			finish()
		}
	}()

	fmt.Println(time.Now())
	select {
	// case <-ctx0.Done():
	// 	fmt.Println("ctx0 happend")
	case <-ctx1.Done():
		fmt.Println("ctx1 happend")
	}
	fmt.Println(time.Now())

	// time.Sleep(3*time.Second)
}
