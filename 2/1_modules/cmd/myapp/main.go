package main

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/rvasily/examplerepo"

	"modules/pkg/user"
	// "gitlab.com/rvasily/go-sm-2021q1/pkg/user"
	// "gitlab.com/rvasily/go-sm-2021q1/2/1_modules/pkg/user"
)

func main() {
	u := user.NewUser(42, "rvasily")
	fmt.Println("my user:", u)

	fmt.Println("const:", examplerepo.FirstName)

}
