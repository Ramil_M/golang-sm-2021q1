Задание №3

Это необязательное задание.

Продолжение предыдущего задания.

В этой части домашки вам надо поработать с БД и написанием тестов.

У нас есть несколько сущностей как в realwordapp (rwa), так и в redditclone. Надо унести их всех в SQL БД. Прием с каждой из сущностей поработать через разную библиотеку - database/sql, sqlx, gorm - чтобы у вас был опыт работы с каждой из них. Какую библиотеку использовать для каких случаев - на ваше усмотрение.

Если вы все правильно сделали в предыдущей части по архитектуре - у вас должны поменяться только репозитории.

В качестве усложненной альтернативы - можно взять не только sql, а sql(users) + mondo(posts) + redis(sessions). В этом случае можно порабоатть только с одной библиотекой с sql, а не со всеми тремя.

После того как вы напишите работу с БД - надо пописать тесты (да, опять, но уже с другим акцентом):

* тесты http-хендлеров - мокаем репозиторий - учимся работать с gomock
* тесты репозиториев - мокаем БД ( в усложненном случае - sql + mongo + redis ) - учимся рабоать с sqlmock
* интеграционные тесты - если вы делали redditclone - за основу можно взять тесты в rwa. Если вы делали rwa - ничего не делаем :)

Задача порабоать с gomock и sqlmock, но не упарываться для достижения 100% покрытия, а именно получить опыт как оно работает.

Уровень покрытия - надо полностью покрыть 1 метод со всеми ошибками. Не всю сущность или пакет, а только 1 метод у структуры. Например, это может быть метод добавления юзера в хттп-хендлере и он же в репозитории.

Надо будет перенести ваш код и предыдущей домашки в соответствующую папку в этой и уже от нее создать МР.