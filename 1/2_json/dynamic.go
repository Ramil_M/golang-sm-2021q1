package main

import (
	"encoding/json"
	"fmt"
	"log"
)

var jsonStr = `[
	{"id": 17, "username": "iivan", "phone": 0},
	[
		{"id": 13, "username": "pete", "phone": 0}
	],
	{"id": "17", "address": "none", "company": "Mail.ru"}
]`

func main() {
	data := []byte(jsonStr)

	var user1 interface{}
	err := json.Unmarshal(data, &user1)
	if err != nil {
		log.Fatalln("bad json", err)
	}
	fmt.Printf("unpacked in empty interface:\n%#v\n\n", user1)

	user2 := map[string]interface{}{
		"id":        42,
		"username":  "rvasily",
		"elems":     []int{1, 2, 3},
		"username2": "rvasily",
		"username3": "rvasily",
	}
	var user2i interface{} = user2
	result, _ := json.Marshal(user2i)
	fmt.Printf("json string from map:\n %s\n", string(result))
}
