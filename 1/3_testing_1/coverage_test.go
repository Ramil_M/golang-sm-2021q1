package main

import (
	"reflect"
	"testing"
)

type TestCase struct {
	Key     string
	User    *User
	IsError bool
}

var cases = []TestCase{
	TestCase{"ok", &User{ID: 27}, false},
	// TestCase{"fail", nil, true},
	TestCase{"not_exist", nil, true},
}

func TestGetUser(t *testing.T) {
	for caseNum, item := range cases {
		u, err := GetUser(item.Key)

		if item.IsError && err == nil {
			t.Errorf("[%d] expected error, got nil", caseNum)
		}
		if !item.IsError && err != nil {
			t.Errorf("[%d] unexpected error %v", caseNum, err)
		}
		if !reflect.DeepEqual(u, item.User) {
			t.Errorf("[%d] wrong results: got %+v, expected %+v",
				caseNum, u, item.User)
		}
	}
}

// go test -v -run "Run/not"
func ZTestRunGetUser(t *testing.T) {
	for _, item := range cases {
		ok := t.Run(item.Key, func(t *testing.T) {
			if testing.Short() { // go test -v -run "Run/not" -short
				t.Skip("skipping test in short mode.")
			}
			u, err := GetUser(item.Key)

			if item.IsError && err == nil {
				t.Errorf("expected error, got nil")
			}
			if !item.IsError && err != nil {
				t.Errorf("unexpected error %v", err)
			}
			if !reflect.DeepEqual(u, item.User) {
				t.Errorf("wrong results: got %+v, expected %+v", u, item.User)
			}
		})
		if !ok {
			break
		}
	}
}

/*
	https://golang.org/pkg/testing/
	https://golang.org/pkg/cmd/go/internal/test/

	go test
	go test -v
	go test -v -run "Run/not"

	go test -short
	go test -failfast

	go test -v -cover
	go test -coverprofile=cover.out && go tool cover -html=cover.out -o cover.html
*/
