Конечно же документация:
* https://golang.org/pkg/net/http/

Дополнительные матералы

* https://gowebexamples.github.io/ - примеры касательно разработки веба, все основное что вам может понадобиться
* https://golang.org/doc/articles/wiki/
* https://astaxie.gitbooks.io/build-web-application-with-golang/
* https://github.com/thewhitetulip/web-dev-golang-anti-textbook/
* https://codegangsta.gitbooks.io/building-web-apps-with-go/content/
* https://www.rzaluska.com/blog/important-go-interfaces/
* https://blog.cloudflare.com/the-complete-guide-to-golang-net-http-timeouts/ - про таймауты
* http://polyglot.ninja/golang-making-http-requests/
* http://tumregels.github.io/Network-Programming-with-Go/

Тесты:
* https://blog.golang.org/cover - расширенная информация о go test -cover
* https://mholt.github.io/json-to-go - позволяет по json сформировать структуру на go, в которую он может быть распакован

Основная и самая важная ссылка, касательно компонентов:
* https://github.com/avelino/awesome-go