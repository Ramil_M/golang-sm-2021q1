package session

import (
	context "context"
	"time"

	"google.golang.org/grpc/metadata"
)

type RepoGrpc struct {
	client AuthClient
}

func NewRepoGrpc(client AuthClient) *RepoGrpc {
	return &RepoGrpc{
		client: client,
	}
}

func (repo *RepoGrpc) Check(ctx context.Context, sessID string) (*Session, error) {
	md := metadata.Pairs(
		"timestamp", time.Now().Format(time.StampNano),
		"service", "crudapp",
	)
	ctx = metadata.NewOutgoingContext(ctx, md)

	authSess, err := repo.client.Check(ctx, &AuthCheckIn{SessKey: sessID})
	if err != nil {
		return nil, err
	}

	return &Session{
		ID:     authSess.GetID(),
		UserID: authSess.GetUserID(),
	}, nil
}

func (repo *RepoGrpc) Create(ctx context.Context, userID uint32) (*Session, error) {
	authSess, err := repo.client.Create(ctx, &AuthUserIn{
		UserID: userID,
	})
	if err != nil {
		return nil, err
	}

	return &Session{
		ID:     authSess.GetID(),
		UserID: authSess.GetUserID(),
	}, nil
}

func (repo *RepoGrpc) DestroyCurrent(ctx context.Context, sessID string) error {
	_, err := repo.client.DestroyCurrent(ctx, &AuthSession{
		ID: sessID,
	})
	return err
}
