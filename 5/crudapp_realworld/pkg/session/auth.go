package session

import (
	context "context"
)

type AuthService struct {
	repo Repo
}

func NewAuthService(repo Repo) *AuthService {
	return &AuthService{
		repo: repo,
	}
}

func (as *AuthService) Check(ctx context.Context, c *AuthCheckIn) (*AuthSession, error) {
	sess, err := as.repo.Check(ctx, c.SessKey)
	if err != nil {
		return nil, err
	}
	return &AuthSession{
		ID:     sess.ID,
		UserID: sess.UserID,
	}, nil
}

func (as *AuthService) Create(ctx context.Context, u *AuthUserIn) (*AuthSession, error) {
	sess, err := as.repo.Create(ctx, u.GetUserID())
	if err != nil {
		return nil, err
	}
	return &AuthSession{
		ID:     sess.ID,
		UserID: sess.UserID,
	}, nil
}

func (as *AuthService) DestroyCurrent(ctx context.Context, s *AuthSession) (*AuthNothing, error) {
	return &AuthNothing{}, as.repo.DestroyCurrent(ctx, s.GetID())
}
