package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"time"

	"crudapp/pkg/session"

	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

var (
	buildHash = "undefined"
	appName   = "crudapp_auth"
)

func AccessLogInterceptor(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (interface{}, error) {
	start := time.Now()
	var requestID string
	md, mdExists := metadata.FromIncomingContext(ctx)
	if mdExists && md != nil && len(md["x-request-id"]) > 0 {
		requestID = md["x-request-id"][0]
	} else {
		requestID = "-"
	}

	// clientContext, err := opentracing.GlobalTracer().Extract(opentracing.HTTPHeaders, traceutils.MetadataReaderWriter{md})
	// var serverSpan opentracing.Span
	// if err == nil {
	// 	serverSpan = opentracing.StartSpan(info.FullMethod, ext.RPCServerOption(clientContext))
	// } else {
	// 	serverSpan = opentracing.StartSpan(info.FullMethod)
	// }
	// defer serverSpan.Finish()

	reply, err := handler(ctx, req)

	log.Printf("[access] %s %s %s '%v'", requestID, time.Since(start), info.FullMethod, err)
	return reply, err
}

func main() {

	jaegerPort := flag.Int("jaeger_port", 8000, "grpc listen port") // 6831
	grpcPort := flag.Int("port", 8083, "grpc listen port")
	flag.Parse()

	zapLogger, _ := zap.NewProduction()
	defer zapLogger.Sync() // flushes buffer, if any
	logger := zapLogger.Sugar()

	logger.Infow("initializing",
		"buildHash", buildHash,
		"appName", appName,
		"jaegerPort", jaegerPort,
	)

	InitTracing()

	server := grpc.NewServer(
		grpc.UnaryInterceptor(otelgrpc.UnaryServerInterceptor()),
		// grpc.UnaryInterceptor(AccessLogInterceptor),
	)
	repo := session.NewRepoMem()
	svc := session.NewAuthService(repo)
	session.RegisterAuthServer(server, svc)

	listenAddr := fmt.Sprintf(":%d", *grpcPort)
	lis, err := net.Listen("tcp", listenAddr)
	if err != nil {
		logger.Fatalw("cant start grpc server",
			"addr", listenAddr,
			"err", err,
		)
	}

	logger.Infow("starting grpc server",
		"addr", listenAddr,
	)
	server.Serve(lis)
}
