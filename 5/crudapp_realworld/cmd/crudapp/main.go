package main

import (
	"database/sql"
	"html/template"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.uber.org/zap"
	"google.golang.org/grpc"

	"go.opentelemetry.io/otel"

	"crudapp/pkg/handlers"
	"crudapp/pkg/items"
	"crudapp/pkg/middleware"
	"crudapp/pkg/session"
	"crudapp/pkg/user"
)

func getMysql(dsn string) *sql.DB {
	// dsn := "root:love@tcp(localhost:3306)/golang?&charset=utf8&interpolateParams=true"
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalln("sql.Open failed", err)
	}
	err = db.Ping() // вот тут будет первое подключение к базе
	if err != nil {
		log.Fatalln("db.Ping failed", err)
	}
	db.SetMaxOpenConns(10)
	return db
}

// http://jmoiron.github.io/sqlx/
func getSqlx(dsn string) *sqlx.DB {
	return sqlx.NewDb(getMysql(dsn), "mysql")
}

var (
	buildHash = "undefined"
	appName   = "crudapp"
)

func main() {
	zapLogger, _ := zap.NewProduction()
	defer zapLogger.Sync() // flushes buffer, if any
	logger := zapLogger.Sugar()

	logger.Infow("initializing",
		"buildHash", buildHash,
		"appName", appName,
	)

	cfg := &Config{}
	_, err := ReadCfg(appName, DefaultCfg, cfg)
	if err != nil {
		logger.Fatalw("cant read cfg",
			"err", err,
		)
	}

	// log.Println(cfg.Jaeger.Agent)
	InitTracing(cfg.Jaeger.Agent)

	templates := template.Must(template.ParseGlob("./templates/*"))

	grcpConn, err := grpc.Dial(cfg.Auth.Addr,
		grpc.WithInsecure(),
		grpc.WithBlock(),
		grpc.WithUnaryInterceptor(otelgrpc.UnaryClientInterceptor()),
	)
	if err != nil {
		logger.Fatalw("cant connect to grpc server",
			"addr", cfg.Auth.Addr,
			"err", err,
		)
	}
	sessRepo := session.NewRepoGrpc(session.NewAuthClient(grcpConn))
	// sessRepo := session.NewRepoMem()

	sm := session.NewManager(sessRepo)

	userRepo := user.NewUserRepo()
	// itemsRepo := items.NewMysqlRepository(getMysql())
	// itemsRepo := items.NewSqlxRepository(getSqlx("root:love@tcp(mysql:3306)/golang?&charset=utf8&interpolateParams=true"))
	itemsRepo := items.NewSqlxRepository(getSqlx(cfg.DB.DSN))
	// itemsRepo := items.NewGormRepository(getGorm())
	// itemsRepo := items.NewPgxRepository(getPostgres())

	userHandler := &handlers.UserHandler{
		Tmpl:     templates,
		UserRepo: userRepo,
		Logger:   logger,
		Sessions: sm,
	}

	handlers := &handlers.ItemsHandler{
		Tmpl:      templates,
		Logger:    logger,
		ItemsRepo: itemsRepo,
	}

	r := mux.NewRouter()
	r.HandleFunc("/", userHandler.Index).Methods("GET")
	r.HandleFunc("/login", userHandler.Login).Methods("POST")
	r.HandleFunc("/logout", userHandler.Logout).Methods("POST")

	r.HandleFunc("/items", handlers.List).Methods("GET")
	r.HandleFunc("/items/new", handlers.AddForm).Methods("GET")
	r.HandleFunc("/items/new", handlers.Add).Methods("POST")
	r.HandleFunc("/items/{id}", handlers.Edit).Methods("GET")
	r.HandleFunc("/items/{id}", handlers.Update).Methods("POST")
	r.HandleFunc("/items/{id}", handlers.Delete).Methods("DELETE")

	router := middleware.Auth(sm, r)
	router = middleware.AccessLog(logger, router)
	router = middleware.Panic(router)

	tracer := otel.Tracer("traceMW")
	traceMW := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx, span := tracer.Start(r.Context(), r.URL.String())
			defer span.End()

			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}

	router = traceMW(router)

	addr := cfg.HTTP.Addr
	logger.Infow("starting http server",
		"addr", addr,
	)
	http.ListenAndServe(addr, router)
}
