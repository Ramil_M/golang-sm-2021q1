package main

import (
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	HTTP struct {
		Addr string
	}
	DB struct {
		DSN string
	}
	Auth struct {
		Addr string
	}
	Jaeger struct {
		Agent string
	}
}

var DefaultCfg = map[string]interface{}{
	"http": map[string]string{
		"addr": ":8084",
	},
	"db": map[string]string{
		"dsn": "dbMysql:3306",
	},
	"auth": map[string]string{
		"addr": "127.0.0.1:8083",
	},
	"jaeger": map[string]string{
		"agent": "127.0.0.1:6831",
	},
}

func ReadCfg(appName string, defaults map[string]interface{}, cfg interface{}) (*viper.Viper, error) {
	v := viper.New()
	for key, value := range defaults {
		v.SetDefault(key, value)
	}
	v.SetConfigName(appName)
	v.SetConfigType("yml")
	v.AddConfigPath("./configs/local/")
	v.AddConfigPath("/etc/")
	v.AutomaticEnv()
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	err := v.ReadInConfig()
	if err != nil {
		return nil, err
	}
	if cfg != nil {
		err := v.Unmarshal(cfg)
		if err != nil {
			return nil, err
		}
	}
	return v, nil
}
