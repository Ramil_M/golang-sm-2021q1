module crudapp

go 1.16

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/golang/mock v1.4.4
	github.com/golang/protobuf v1.5.1
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/schema v1.1.0
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jmoiron/sqlx v1.3.1
	github.com/lib/pq v1.10.0 // indirect
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/spf13/viper v1.7.1
	go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc v0.19.0
	go.opentelemetry.io/otel v0.19.0
	go.opentelemetry.io/otel/exporters/stdout v0.19.0
	go.opentelemetry.io/otel/exporters/trace/jaeger v0.19.0
	go.opentelemetry.io/otel/sdk v0.19.0
	go.uber.org/multierr v1.5.0 // indirect
	go.uber.org/zap v1.12.0
	google.golang.org/grpc v1.36.0
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
	gorm.io/gorm v1.21.3
)
